drop table if exists Tour cascade;
drop table if exists City cascade;
drop table if exists Review cascade;
drop table if exists "CityToTour" cascade;

create table Tour
(
    name text constraint "PK_name_tour" primary key,
    price int
);

create table City
(
    name text constraint "PK_name_city" primary key,
    founded text
);

create table Review
(
    id bigint generated always as identity
        constraint "PK_id" primary key,
    comment text,
    ratio integer not null,
    tour text constraint "FK_tour_review" references tour not null
);

create table "CityToTour"
(
    "NameTour" text not null,
    "NameCity" text not null,
        constraint "FK_nameCity_toCity" foreign key("NameCity") references City,
        constraint "FK_nameTour_toTour" foreign key("NameTour") references Tour
);

alter table "CityToTour"
    add constraint "UQ_CityToTour" unique("NameTour", "NameCity");

insert into Tour
values  ('Спокойной ночи, малыши!', 300),
        ('Хиты Моргенштерна', 666);

insert into City
values  ('Владик', '1300'),
        ('Новосиб', '500'),
        ('Самара', '1600');

insert into "CityToTour"
values  ('Спокойной ночи, малыши!', 'Владик'),
        ('Спокойной ночи, малыши!', 'Новосиб'),
        ('Хиты Моргенштерна', 'Новосиб'),
        ('Хиты Моргенштерна', 'Самара');

insert into Review(comment, ratio, tour)
values  ('Лайк', 5, 'Хиты Моргенштерна'),
        ('Дизлайк', 1, 'Хиты Моргенштерна');

select "NameTour", "NameCity", founded, comment, ratio
from Tour
    join "CityToTour" on Tour.name = "CityToTour"."NameTour"
    join City on City.name = "CityToTour"."NameCity"
    left join Review on Tour.name = Review.tour
