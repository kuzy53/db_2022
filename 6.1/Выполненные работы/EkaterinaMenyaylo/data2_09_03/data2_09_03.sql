drop table if exists Tour cascade;

drop table if exists Rewiew cascade;

create table tour
(
    name text,
    price float

);
create table rewiew
(
    comment  text,
    ratio int,
    "tourname" text
);

alter table tour
    add constraint "PK_Name" primary key ("name");

alter table rewiew
    add constraint "FK_tourname" foreign key ("tourname") references tour ("name");

insert into tour
values ('forest',12),
       ('beatch',11);

insert into rewiew(comment, ratio, tourname)
    values ('top', 4, 'forest'),
           ('nice',5,'forest'),
           ('t', 2, 'beatch'),
           ('n',1,'beatch');

select *
from tour
    join rewiew on name = "tourname";


