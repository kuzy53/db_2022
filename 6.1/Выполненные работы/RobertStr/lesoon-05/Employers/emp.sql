drop table if exists "Employe" cascade ;
drop table if exists "Office" cascade ;

create table "Employe"
(
    id  bigint generated always as identity
        constraint "PK_id" primary key,
    name text,
    experience int,
    chief bigint,
    office bigint
);
create  table "Office"
(
   id  bigint generated always as identity
    constraint "Pk_codeoficce" primary key,
    code int ,
    space  int
);

alter table "Employe"
    add constraint "FK_idchief" foreign key (chief) references "Employe" (id);
alter table "Employe"
     add constraint "Fk_ofice" foreign key (office) references "Office"(id);

insert into "Office" (code, space)
values (101,14),
       (301,14),
       (501,15),
       (1001,35);
insert into "Employe" (name, experience, chief, office)
values ('robert',12,Null,1),
       ('ars',2,1,2),
       ('a',2,2,3),
       ('av',2,2,2),
       ('fifrr',4,1,4);

select "Employe".name,"Employe".office,"Office".id,"Employe".chief,rabotnik.name,rabotnik.office,"Employe"."office" from "Employe"
    left join "Office" on "Office".id = "Employe"."office"
    left join "Employe"  as rabotnik on "Employe".id  = "rabotnik".chief