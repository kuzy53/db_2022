drop table if exists Tour cascade;

drop table if exists Rewiew cascade;

create table tour
(
    name text,
    price float

);
create table rewiew
(
    comment  text,
    ratio int,
    "tourname" text
);

alter table tour
    add constraint "PK_Name" primary key ("name");

alter table rewiew
    add constraint "FK_tourname" foreign key ("tourname") references tour ("name");

insert into tour
values ('aboba',12),
       ('abiba',11);

insert into rewiew(comment, ratio, tourname)
    values ('aaaaaaaaaa', 4, 'aboba'),
           ('bbbbbbbb',5,'aboba'),
           ('a', 2, 'abiba'),
           ('b',1,'abiba');

select *
from tour
    join rewiew on name = "tourname";


