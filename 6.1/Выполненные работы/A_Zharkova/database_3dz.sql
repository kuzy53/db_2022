drop table if exists users cascade;
drop table if exists settings cascade;

create table users
(
    nickname text primary key,
    first_name text,
    last_name text
);

create table settings
(
    font_size int,
    color_scheme text,
    user_name text unique references users
);


insert into users (nickname, first_name, last_name)
values ('A_Zharkova_', 'Anastasia', 'Zharkova'),
       ('I_Ivanov', 'Ivan', 'Ivanov');

insert into settings (font_size, color_scheme, user_name)
values (11, 'blue', 'A_Zharkova_'),
       (8, 'black', 'I_Ivanov');

select nickname, font_size, color_scheme
from users
    join settings on nickname = user_name;