drop table if exists tour cascade;
drop table if exists review cascade;

create table tour
(
    name text primary key ,
    price numeric

);
create table review
(
    comment  text,
    ratio int,
    tourname text references tour
);



insert into tour
values ('Абхазия', 5000),
       ('Красная поляна', 3000);

insert into review(comment, ratio, tourname)
    values ('Всё понравилось', 5, 'Абхазия'),
           ('Но был лохой ресторан', 3, 'Абхазия'),
           ('Было классно!!!', 5, 'Красная поляна'),
           ('Неплохое место', 4, 'Красная поляна');

select name, comment
from tour
    join  review on name = tourname;