import psycopg2
import psycopg2.extras
from psycopg2.extras import RealDictCursor


class Database:
    def __init__(self):
        self.conn = psycopg2.connect(database="postgres", user='postgres',
                                     password='changeme', host='localhost', port=999, cursor_factory=RealDictCursor)

    def select(self):
        cursor = self.conn.cursor()
        cursor.execute("""select nickname, first_name, last_name, font_size, color_scheme
        from users join Settings  on nickname = users_nickname;""")
        self.conn.commit()
        return cursor.fetchall()


class User:
    def __init__(self, nickname, first_name, last_name, setting):
        self.nickname = nickname
        self.first_name = first_name
        self.last_name = last_name
        self.setting = setting


    def print_users(self):
        print("nickname: ", self.nickname)
        print("first_name: ", self.first_name)
        print("last_name: ", self.last_name)
        self.setting.print_setting()


class Setting:
    def __init__(self, font_size, color_scheme):
        self.font_size = font_size
        self.color_scheme = color_scheme

    def set_user(self, user):
        self.user = user
        
    def print_setting(self):
        print("font_size: ", self.font_size)
        print("color_scheme: ", self.color_scheme)


db = Database()

a = db.select()
users = []
settings = []


for i in a:
    dict_i = dict(i)
    setting = Setting(dict_i["font_size"],dict_i["color_scheme"])
    user = User(dict_i["nickname"], dict_i["first_name"], dict_i["last_name"], setting)
    setting.set_user(user)
    users.append(user)
    settings.append(setting)

for i in users:
    i.print_users()

