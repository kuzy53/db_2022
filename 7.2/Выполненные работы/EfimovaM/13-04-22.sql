drop table if exists tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists "cityToTour" cascade;

create table tour
(
  name varchar(128) constraint "PK_tour_Name" primary key
);

create table city
(
  name varchar(128) constraint "PK_city_Name" primary key
);

create table review
(
  comment varchar(128),
  rating integer not null,
  tour varchar(128) constraint "FK_tour_Review" references tour(name) not null
);

create table "cityToTour"
(   "tourName" varchar(128) not null,
  "cityName" varchar(128) not null,
  constraint "FK_tourName_toTour" foreign key("tourName") references tour(name),
  constraint "FK_cityName_toCity" foreign key("cityName") references city(name)
  
);
alter table "cityToTour"
  add constraint "UQ_cityToTour" unique("tourName", "cityName");

insert into tour
values  ('В гостях у Хаски'),
    ('Хиты карелии');

insert into city
values  ('Приозерск'),
    ('Сартовала'),
    ('Валаам');

insert into "cityToTour"
values  ('В гостях у Хаски', 'Приозерск'),
    ('В гостях у Хаски', 'Сартовала'),
    ('Хиты карелии', 'Сартовала'),
    ('Хиты карелии', 'Валаам');

insert into review
values  ('Отличный тур', 5, 'Хиты карелии'),
    ('Не понравилось', 2, 'Хиты карелии');

select "tourName", "cityName", comment, rating from tour 
        join "cityToTour" on tour.name = "cityToTour"."tourName"
        join city on city.name = "cityToTour"."cityName"
        join review on tour.name = review.tour;

alter table tour add column price numeric(15, 6);
alter table city add column founded text;
alter table review add column id int;

update tour
set price = 15650
where name = 'Хиты карелии';

update tour
set price = 9870
where name = 'В гостях у Хаски';

update city
set founded = 1295
where name = 'Приозерск';

update city
set founded = 1407
where name = 'Валаам';

update city
set founded = 1468
where name = 'Сартовала';

update review
set id = 1
where comment = 'Отличный тур' and rating = 5 and tour = 'Хиты карелии';

update review
set id = 2
where comment = 'Не понравилось' and rating = 2 and tour = 'Хиты карелии';
