drop table if exists folder cascade;
drop table if exists file cascade;

create table folder
(
    id int generated always as identity constraint "PK_id_folder" primary key,
    name text not null,
    size int,
    parent_id int constraint "FK_idParentFolder" references folder,
  unique (name, parent_id)
);

create table file
(
    id int generated always as identity constraint "PK_id_file" primary key,
    name text not null,
    size int,
    folder_id int constraint "FK_folder_id" references folder,
  unique (name, folder_id)
);

insert into folder (name, size, parent_id)
values 	('db_2022', 57, null),
		    ('7.1', 5.5, 1),
		    ('7.2', 19, 1),
		    ('ВР', 15, 3),
		    ('ДЗ', 7.5, 3),
		    ('ЛР', 11.5, 3),
		    ('Pataraya', 5, 4),
		    ('Iakovleva', 2, 4),
		    ('Khabibullin', 3.5, 4),
		    ('motrikala',6,  4);

insert into file (name, size, folder_id)
values  ('02.03.2022', 2, 5),
		    ('15.02.2022', 2, 5),
		    ('18.02.2022',2, 5),
		    ('22.02.2022',3, 5),
		    ('Спецификация', 2, 6),
		    ('18_02.sql',2, 7),
		    ('22_02.sql',3, 7);

select parent.id as "id_Folder", parent.name, parent.size, parent.parent_id, file.id as "child_id", file.name as "child_name",
  file.folder_id as "file_Parent_id", child.id as "child_id", child.name as "child_name", child.parent_id as "parent_id"
from folder as parent
    left join file
      on parent.id  = file.folder_id
     left join folder as child
      on parent.id = child.parent_id;
