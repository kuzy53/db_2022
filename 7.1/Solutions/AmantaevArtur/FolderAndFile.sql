drop table if exists Folder cascade;
drop table if exists File cascade;

create table Folder
(
    id               int generated always as identity
        constraint "PK_Folder_id" primary key,
    size             numeric,
    name             text not null,
    id_parent_folder int default null
        constraint "FK_id_parent"
            references folder,
    unique (name, id_parent_folder)
);

create table File
(
    id        int generated always as identity
        constraint "PK_File_id" primary key,
    size      numeric,
    name      text not null,
    id_folder int  not null
        constraint "FK_folder_id" references folder,
    unique (name, id_folder)
);

insert into Folder (size, name)
values (100, 'Home');

insert into Folder (size, name, id_parent_folder)
values (40, 'Gallery', 1),
       (10, 'Document', 1);

insert into Folder (size, name, id_parent_folder)
values (10, 'CatsPhotos', 2),
       (10, 'Abstracts', 3);

insert into File (size, name, id_folder)
values (5, '123.png', 2),
       (10, '1.jpg', 2),
       (80, 'qwe.txt', 3),
       (90, '.gitignore', 1),
       (5, '456.png', 4),
       (5, 'aaa.doc', 5);

select parent.id              as "idFolder",
       parent.name,
       parent.size,
       parent.id_parent_folder,
       file.id                as "childId",
       file.name              as "childName",
       file.id_folder         as "fileParentId",
       child.id               as "fileId",
       child.name             as "fileName",
       child.id_parent_folder as "parentId"
from folder as parent
         left join file
                   on parent.id = file.id_folder
         left join folder as child
                   on parent.id = child.id_parent_folder;

-- вывод всех данных в одной таблице
select Folder.name, Folder.size, childFolder.name, childFolder.size, File.name, File.size
from Folder
         left join Folder as childFolder on Folder.id = childFolder.id_parent_folder
         left join File on Folder.id = File.id_folder;

-- вывод папок и их дочерних папок
select Folder.name, Folder.size, childFolder.name, childFolder.size
from Folder
         left join Folder as childFolder on Folder.id = childFolder.id_parent_folder;

-- вывод папок и файлов лежащих в этой папке
select Folder.name, Folder.size, File.name, File.size
from Folder
         left join File on Folder.id = File.id_folder;

-- вывод вложенных папок и файлов используя рекурсию
with recursive foldersAndFilesWithRecursion as (select Folder.id, Folder.name as folder_name, parent_folder.name as parent_folder_name, File.name as file_name, 1 as depth
                           from Folder
                                    left join File on File.id_folder = Folder.id
                                    left join Folder as parent_folder on parent_folder.id = Folder.id_parent_folder
                           where Folder.id = 1
                           union
                           select subFolder.id,
                                  subFolder.name as folder_name,
                                  parent_folder.name as parent_folder_name,
                                  File.name as file_name,
                                  depth + 1 as depth
                           from Folder as subFolder
                                    join foldersAndFilesWithRecursion on foldersAndFilesWithRecursion.id = subFolder.id_parent_folder
                                    left join File on File.id_folder = subFolder.id
                                    left join Folder as parent_folder on parent_folder.id = subFolder.id_parent_folder
                           where depth + 1 <= 3)
select *
from foldersAndFilesWithRecursion;