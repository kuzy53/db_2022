drop table if exists "City" cascade;
drop table if exists "Tour" cascade;
drop table if exists "Review" cascade;
drop table if exists "TourToCity" cascade;

create table "City"
(
    name    varchar(128)
        constraint city_pk
            primary key,
    founded varchar(128)
);

create table "Tour"
(
    name  varchar(128)
        constraint tour_pk
            primary key,
    price numeric
);

create table "Review"
(
    comment   varchar(128),
    rating    integer,
    id        integer not null
        constraint review_pk
            primary key,
    tour_name varchar(128)
        constraint "FK_tourName"
            references "Tour"
);

insert into "Tour" (name, price)
values ('В гостях у хаски', 1000),
       ('Хиты карелии', 2000);

insert into "Review" (comment, rating, id, tour_name)
values ('Отличный тур!', 5, 1, 'Хиты карелии');

insert into "Review" (comment, rating, id, tour_name)
values ('Не понравилось', 2, 2, 'Хиты карелии');

create table "TourToCity"
(
    tourName varchar(128),
    city_name varchar(128)
);

alter table "TourToCity"
    add constraint "FK_tourName" foreign key (tourName) references "Tour" (name);

alter table "TourToCity"
    add constraint "FK_cityName" foreign key (city_name) references "City" (name);

alter table "TourToCity"
    add constraint "UQ_tourToCity" unique (tourName, city_name);

alter table "TourToCity"
    alter column tourName set not null;

alter table "TourToCity"
    alter column city_name set not null;

insert into "City" (name, founded)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);
insert into "TourToCity" (tourName, city_name)
values ('В гостях у хаски', 'Приозерск'),
       ('Хиты карелии', 'Сортавала'),
       ('Хиты карелии', 'Валаам');

select "Tour".name, "City".name, price, comment, rating
from "Tour"
         join "Review" on "Tour".name = "Review".tour_name
         join "TourToCity" on "Tour".name = "TourToCity".tourName
         join "City" on "TourToCity".city_name = "City".name
where tourName = 'Хиты карелии';

select "tourName", "cityName", price, comment, ration
from "City"
         join "TourToCity" on "City".name = "cityName"
         join "Tour" on "tourName" = "Tour".name
         left join "Review" on "Tour".name = "Review".tourname