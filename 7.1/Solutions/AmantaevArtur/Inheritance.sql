drop table if exists Dialog cascade;
drop table if exists dialog_to_operator cascade;
drop table if exists Message cascade;
drop table if exists Knowledge_base cascade;
drop table if exists Operator cascade;
drop table if exists "User" cascade;

create table "User"
(
    id   int generated always as identity primary key,
    name text
);

create table Operator
(
    id   int generated always as identity primary key,
    name text
);

create table Dialog
(
    id         int generated always as identity primary key,
    date_start timestamp,
    date_end   timestamp,
    user_id  int references "User"
);

create table Knowledge_base
(
    id   int generated always as identity primary key,
    text text
);

create table dialog_to_operator
(
    dialog_id   int references Dialog   not null,
    operator_id int references operator not null,
    unique (dialog_id, operator_id)
);

create table Message
(
    id                   int generated always as identity primary key,
    canal                text,
    content              text,
    date_send            timestamp,
    dialog_id            int references dialog,
    knowledge_message_id int references Knowledge_base,
    operator_message_id  int references operator,
    user_id            int references "User",
    type                 text
);

insert into "User" (name)
values ('user1');

insert into Operator (name)
values ('Operator1'),
       ('Operator2'),
       ('Operator3'),
       ('Operator4');

insert into Dialog (date_start, date_end, user_id)
values ('2022-04-13 09:08:00', '2022-05-13 09:08:00', 1),
       ('2022-04-01 09:08:00', '2022-07-01 09:08:00', 1);

insert into dialog_to_operator (dialog_id, operator_id)
values (1, 1),
       (1, 2),
       (2, 3),
       (2, 4);

insert into Message (type, canal, content, date_send, user_id, dialog_id)
values ('user', 'canal1', 'blablabla', '2022-06-13 00:00:00', 1, 1),
       ('user', 'canal1', 'aaaaaaa', '2022-07-03 00:00:00', 1, 1),
       ('user', 'canal2', 'yeeeee', '2022-07-06 00:00:00', 1, 2),
       ('user', 'canal2', 'wooooo', '2022-07-10 00:00:00', 1, 2);

insert into Message (type, content, date_send, operator_message_id, dialog_id)
values ('operator', 'qwerty', '2022-06-14 00:00:00', 1, 1),
       ('operator', 'asdf', '2022-07-04 00:00:00', 1, 1),
       ('operator', 'zxcv', '2022-07-07 00:00:00', 2, 1),
       ('operator', 'oooooo', '2022-07-08 00:00:00', 2, 1),
       ('operator', 'zzzzzzz', '2022-07-10 00:00:00', 3, 2),
       ('operator', 'lililili', '2022-07-12 00:00:00', 4, 2);

insert into Knowledge_base (text)
values ('0_0'),
       (':)');

insert into Message (type, knowledge_message_id, date_send, operator_message_id, dialog_id)
values ('operator', 1, '2022-07-13 00:00:00', 3, 2),
       ('operator', 2, '2022-07-14 00:00:00', 4, 2);


select dialog.id,
       "User".name,
       message.content
from dialog
         join "User" on dialog.user_id = "User".id
         join Message on Message.user_id = "User".id
where dialog.id = 1
union
select dialog.id,
       operator.name,
       message.content
from message
         join operator on message.operator_message_id = operator.id
         join dialog on dialog.id = message.dialog_id
where message.content is not null
  and dialog.id = 1
union
select dialog.id,
       operator.name,
       text
from message
         join operator on message.operator_message_id = operator.id
         join dialog on dialog.id = message.dialog_id
         join Knowledge_base on message.knowledge_message_id = Knowledge_base.id
where dialog.id = 1;
