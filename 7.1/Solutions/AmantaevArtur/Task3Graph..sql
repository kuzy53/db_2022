drop table if exists "User" cascade;
drop table if exists "Tweet" cascade;
drop table if exists "UserToUser" cascade;


create table "User"
(
    id    int generated always as identity primary key,
    name  text,
    email text
);

create table "Tweet"
(
    id          int generated always as identity primary key,
    content     text,
    date_public timestamp,
    user_id     int references "User"
);

create table "UserToUser"
(
    id_subscription int references "User" not null,
    id_subscriber   int references "User" not null,
    unique (id_subscription, id_subscriber)
);

insert into "User" (name, email)
values ('user1', 'u1@gmail.com'),
       ('user2', 'u2@gmail.com'),
       ('user3', 'u3@gmail.com'),
       ('user4', 'u4@gmail.com'),
       ('user5', 'u5@gmail.com');

insert into "Tweet" (content, date_public, user_id)
values ('tweet1', '2022-04-09 09:52:11', 2),
       ('tweet2', '2022-04-10 09:52:11', 2),
       ('tweet3', '2022-04-08 09:52:11', 3),
       ('tweet4', '2022-04-09 19:52:11', 3);

insert into "UserToUser" (id_subscription, id_subscriber)
values (2, 1),
       (3, 1),
       (1, 4),
       (1, 5);

select "User".id, "User".name as user_id, "UserToUser".id_subscription, uu.name as name_subscription, "Tweet".content, utu.id_subscriber,
       user_follower.name as name_subscriber from "User"
left join "UserToUser" on "User".id = "UserToUser".id_subscriber
left join "Tweet" on "Tweet".user_id = "UserToUser".id_subscription
left join "User" as uu on "UserToUser".id_subscription = uu.id
left join "UserToUser" as utu on "User".id = utu.id_subscription
left join "User" as user_follower on user_follower.id = utu.id_subscriber
where "User".id = 1;