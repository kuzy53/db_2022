-- HW many-to-many

drop table if exists tour cascade;
drop table if exists review cascade;
drop table if exists city cascade;
drop table if exists city_to_tour cascade;

create table tour(
	name text primary key,
	price numeric
);

create table city(
	name text primary key,
	founded text
);

create table review(
	id int generated always as identity primary key,
	tour_name text REFERENCES tour,
	comment text,
	raiting int
);

create table city_to_tour(
	city_name text references city not null,
	tour_name text references tour not null,
	unique(city_name, tour_name)
);

insert into tour (name, price)
values
('В гостях у хаски', 1000),
('Хиты карелии', 2000);

insert into city (name, founded)
values
('Приозерск', '1295'),
('Валаам', '1295'),
('Сортавала', '1295');

insert into review (tour_name, comment, raiting)
values
('Хиты карелии', 'Отличный тур!', 5),
('Хиты карелии', 'Не понравилось.', 2);

insert into city_to_tour (city_name, tour_name)
values
('Приозерск', 'В гостях у хаски'),
('Сортавала', 'В гостях у хаски'),
('Сортавала', 'Хиты карелии'),
('Валаам', 'Хиты карелии');


select tour.name as tour_name, city.name as city_name, city.founded, review.comment, review.raiting
from tour
         left join city_to_tour on tour.name = city_to_tour.tour_name
         left join city on city.name = city_to_tour.city_name
         left join review on review.tour_name = tour.name;
