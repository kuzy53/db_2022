drop table if exists folders cascade;
drop table if exists files cascade;

create table folders(
id int generated always as identity primary key,
parent_id int references folders,
name text,
unique(parent_id, name)
);

create table files(
id int generated always as identity primary key,
parent_folder_id int references folders not null,
name text,
size int,
unique(parent_folder_id, name)
);

insert into folders (parent_id, name)
values 
(null, 'корень'),
(1, 'дочь1'),
(1, 'дочь2'),
(2, 'дочь-дочь1'),
(3, 'дочь-дочь2');

insert into files (parent_folder_id, name, size)
values 
(1, 'файл1', 100),
(2, 'файл2', 101),
(3, 'файл3', 102),
(4, 'файл4', 103);

select parent.id, parent.name, child.name as child_name, files.name as file_name, files.size
from folders as parent
  left join folders as child on parent.id = child.parent_id
  left join files on parent.id = files.parent_folder_id;
