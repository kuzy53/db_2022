drop table if exists users cascade;
drop table if exists tweets cascade;

create table users(
id int generated always as identity primary key,
name text,
mail text
);

create table tweets(
id int generated always as identity primary key,
user_id int references users not null,
content text,
publication_date timestamp
);

create table user_to_user(
subscriber_id int references users not null,
responder_id int references users not null,
unique(subscriber_id, responder_id)
);

insert into users (name, mail)
values 
('Петя', 'superPetr@milo.net'),
('Максим', 'maksimus@pravda.ru'),
('Аркадий', 'NotArkadij@siri.com'),
('Иннокентий', 'Kesha@parrot.yes'),
('Егор', 'TheBestAuthor@mail.ru');


insert into user_to_user (subscriber_id, responder_id)
values 
(1, 2),
(1, 3),
(4, 1),
(5, 1);

insert into tweets (user_id, content, publication_date)
values 
(2, 'Жизнь хороша', '10-Jun-2017 10:00'),
(2, 'SQL - это вам не noSQL', '16-Sep-2018 09:30'),
(3, '[рандомный твит]', '25-May-2016 10:30'),
(3, 'Эх', '05-Mar-2018 11:15');


select users.id, name, mail, content, publication_date
from users 
	join user_to_user on (users.id = user_to_user.responder_id and user_to_user.subscriber_id = 1) 
					or (users.id = user_to_user.subscriber_id and user_to_user.responder_id = 1)
	left join tweets on tweets.user_id = users.id;
        
         
         
         
         
         
         
