package javaCode;

import java.util.ArrayList;

public class Holder {
    String name;
    String phone;
    ArrayList<Equipment> equipments = new ArrayList<>();

    public Holder(String name, String phone){
        this.name = name;
        this.phone = phone;
    }

    public String getName(){
        return this.name;
    }

    public boolean addEquipment(Equipment equipment){
        this.equipments.add(equipment);
        return true;
    }

    public void printEquipment(){
        for (Equipment equipment : equipments){
            System.out.println(equipment.getTitle());
        }
    }
}
