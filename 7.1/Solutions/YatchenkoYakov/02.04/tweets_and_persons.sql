drop table if exists persons cascade;
drop table if exists tweets cascade;
drop table if exists followers cascade;
drop table if exists persons_to_tweets cascade;

create table persons
(
    id   int generated always as identity primary key,
    name text,
    mail text
);

create table tweets
(
    id      int generated always as identity primary key,
    content text,
    date    timestamp,
    author_id int references persons
);

create table followers
(
    follower_id int references persons not null,
    person_id   int references persons not null,
    unique (person_id, follower_id)
);


insert into persons (name, mail)
values ('user1', 'user1@post.com'),
       ('user2', 'user2@post.com'),
       ('user3', 'user3@post.com'),
       ('user4', 'user4@post.com'),
       ('user5', 'user5@post.com');

insert into tweets (content, date, author_id)
values ('Hello?', '12/12/2000', 2),
       ('Is there anybody in there?', '11/11/2001', 2),
       ('Just nod if you can hear me', '10/10/2002', 3),
       ('Is there anyone home?', '9/9/2003', 3);

insert into followers
values (1, 2),
       (1, 3),
       (4, 1),
       (5, 1);

select media.name, tweets.content, tweets.date, subs.name
from persons
         join followers as channel on persons.id = channel.follower_id
         join persons as media on channel.person_id = media.id
         join tweets on media.id = tweets.author_id
         join followers on persons.id = followers.person_id
         join persons as subs on followers.follower_id = subs.id
where persons.id = 1


