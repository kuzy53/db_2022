drop table if exists users cascade;
drop table if exists dialogs cascade;
drop table if exists messages cascade;
drop table if exists dialogs_to_users cascade;
drop table if exists possible_answers cascade;

create table dialogs
(
    id         int generated always as identity primary key,
    start_date timestamp,
    end_date   timestamp
);

create table users
(
    id   int generated always as identity primary key,
    type text,
    name text
);

create table possible_answers
(
    id          int generated always as identity primary key,
    answer_text text
);

create table dialogs_to_users
(
    dialog_id int references dialogs not null,
    user_id   int references users   not null,
    unique (dialog_id, user_id)
);


create table messages
(
    id                 int generated always as identity primary key,
    type               text,
    channel            text,
    text               text,
    send_date          timestamp,
    dialog_id          int references dialogs not null,
    user_id            int references users   not null,
    possible_answer_id int references possible_answers
);

insert into dialogs (start_date, end_date)
values ('10/10/2012', '03/04/2017'),
       ('02/02/2015', '09/12/2018');

insert into users (type, name)
values ('user', 'User1'),
       ('moder', 'Moder1'),
       ('moder', 'Moder2');

insert into possible_answers (answer_text)
values ('possible_answer1'),
       ('possible_answer2'),
       ('possible_answer3'),
       ('possible_answer4');

insert into dialogs_to_users
values (1, 1),
       (1, 2),
       (1, 3),
       (2, 1),
       (2, 2),
       (2, 3);


insert into messages(type, channel, text, send_date, dialog_id, user_id, possible_answer_id)
values ('user_message', 'first_channel', 'some_text_of_first_message', '11/11/2016', 1, 1, null),
       ('user_message', 'first_channel', 'some_text_of_second_message', '12/11/2016', 1, 1, null),
       ('moder_message', null, 'some_text_of_third_message', '12/11/2016', 1, 2, 1),
       ('moder_message', null, 'some_text_of_fourth_message', '10/11/2016', 1, 2, 2),
       ('moder_message', null, 'some_text_of_fifth_message', '10/11/2016', 1, 3, 4),
       ('moder_message', null, 'some_text_of_sixth_message', '11/11/2016', 1, 3, 1),
       ('user_message', 'second_channel', 'some_text_of_seventh_message', '11/11/2016', 2, 1, null),
       ('user_message', 'second_channel', 'some_text_of_eighth_message', '12/11/2016', 2, 1, null),
       ('moder_message', null, 'some_text_of_ninth_message', '12/11/2016', 2, 2, 1),
       ('moder_message', null, 'some_text_of_tenth_message', '10/11/2016', 2, 2, 2),
       ('moder_message', null, 'some_text_of_eleventh_message', '10/11/2016', 2, 3, 4),
       ('moder_message', null, 'some_text_of_twelfth_message', '11/11/2016', 2, 3, 1);

select dialogs.start_date, users.name, users.type, messages.text, messages.send_date, possible_answers.answer_text
from dialogs
         left join dialogs_to_users dtu on dialogs.id = dtu.dialog_id
         left join users on dtu.user_id = users.id
         left join messages on users.id = messages.user_id
         left join possible_answers on messages.possible_answer_id = possible_answers.id




