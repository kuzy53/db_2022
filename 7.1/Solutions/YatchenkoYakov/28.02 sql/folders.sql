drop table if exists folders cascade;
drop table if exists files cascade;

create table folders
(
    id        int generated always as identity primary key,
    size      int,
    name      text,
    parent_id int references folders,
    unique (name, parent_id)
);

create table files
(
    id        int generated always as identity primary key,
    size      int  not null,
    name      text not null,
    folder_id int references folders,
    unique (folder_id, name)
);


insert into folders (name, size, parent_id)
values ('My_folder', 2787, null),
       ('first_level_1', 1324, 1),
       ('first_level_2', 344, 1),
       ('second_level_1', 332, 2),
       ('second_level_2', 654, 3);


insert into files (name, size, folder_id)
values ('file_main', 2787, 1),
       ('file_first_level_1', 1324, 2),
       ('file_first_level_2', 344, 2),
       ('file_second_level_1', 332, 4);

select parent.name as "folder",
       parent.size as size,
       child.name  as "subfolder",
       child.size  as subfolders_size,
       files.name  as content,
       files.size  as content_size


from folders as "parent"
         left join folders as "child" on child.parent_id = parent.id
         left join files on files.folder_id = parent.id
;

with recursive parent as (select folders.id,
                                 folders.name as folder_name,
                                 subfolders.name as subfolder,
                                 files.name   as file_name,
                                 1            as depth
                          from folders
                                   left join folders as subfolders on folders.id = subfolders.parent_id
                                   left join files on files.folder_id = folders.id
                          where folders.id = 1
                          union
                          select subfolders.id, subfolders.name, sub_sub.name as subfolder, files.name, depth + 1 as depth
                          from parent
                                   left join folders as subfolders on parent.id = subfolders.parent_id
                                   left join folders as sub_sub on subfolders.id = sub_sub.parent_id
                                   left join files on subfolders.id = files.folder_id
                          where depth + 1 <= 3)
select *
from parent;
