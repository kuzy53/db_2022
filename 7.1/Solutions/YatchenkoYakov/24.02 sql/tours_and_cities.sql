drop table if exists tours cascade;
drop table if exists cities cascade;
drop table if exists reviews cascade;
drop table if exists tours_to_cities cascade;

create table tours
(
    name  text primary key not null,
    price int
);
create table cities
(
    name         text primary key not null,
    founded_date int
);
create table reviews
(
    comment text,
    ratio   int,
    id      int,
    tour    text references tours
);


create table tours_to_cities
(
    tour_name text references tours,
    city_name text references cities,
    unique (tour_name, city_name)
);

insert into tours
values ('В гостях у хаски', 3500),
       ('Хиты Карелии', 4500);

insert into reviews (id, comment, ratio, tour)
values (1, 'Отличный тур', 5, 'Хиты Карелии'),
       (2, 'Не понравилось', 2, 'Хиты Карелии');

insert into cities (name, founded_date)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);

insert into tours_to_cities
values ('В гостях у хаски', 'Приозерск'),
       ('В гостях у хаски', 'Сортавала'),
       ('Хиты Карелии', 'Сортавала'),
       ('Хиты Карелии', 'Валаам');

select tours.name as "tour", price, city_name as "city", founded_date, comment, ratio
from tours
         join tours_to_cities on tours.name = tour_name
         join cities on city_name = cities.name
         join reviews on tour = tours.name
where tour = 'Хиты Карелии';


