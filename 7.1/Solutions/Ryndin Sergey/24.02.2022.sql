drop table if exists tour cascade; 
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists "TourToCity" cascade;

create table tour
(
	name text constraint "PK_tourName" primary key,
	price numeric
);

create table city
(
	name text constraint "PK_cityName" primary key,
	founded text
);

create table review 
(
	id bigint generated always as identity constraint "PK_id" primary key,
	comment text,
	ratio numeric,
	"tourName" text constraint "FK_tourName" references tour (name)
);

create table "TourToCity"
(
	"tourName" text NOT NULL constraint "FK_TourName" references tour,
	"cityName" text NOT NULL constraint "FK_CityName" references city,
	constraint "UQ_TourToCity"
        unique ("tourName", "cityName")
);

insert into tour (name, price)
values ('В гостях у хаски', 4000),
	   ('Хиты карелии', 3000);
	   
insert into city (name, founded)
values ('Приозерск', '1295'),
	   ('Сортавала', '1468'),
	   ('Валаам', '1407');
	  
insert into "TourToCity" ("tourName", "cityName")
values ('В гостях у хаски','Приозерск'),
	   ('В гостях у хаски','Сортавала'),
	   ('Хиты карелии','Сортавала'),
	   ('Хиты карелии','Валаам');
	   
	   
insert into review (comment, ratio, "tourName")
values ('Отличный тур!', 5, 'Хиты карелии'),
	   ('Не понравилось.', 2 , 'Хиты карелии');
	   
select price, comment, ratio
	from tour
		join review on tour.name = review."tourName"  
		
	  
	  
	  
	  
	  
	  
	  