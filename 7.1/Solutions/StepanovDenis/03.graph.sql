drop table if exists person cascade;
drop table if exists tweet cascade;
drop table if exists person_to_person cascade;

create table person
(
    id int generated always as identity primary key,
    name text,
    email text
);

create table tweet
(
    id int generated always as identity primary key,
    content text,
    date_publisher timestamp,
    fk_person_id int references person(id) not null
);

create table person_to_person
(
    person_profile int references person(id) not null,
    person_subscription int references person(id ) not null,
    unique (person_profile, person_subscription)
);

insert into person (name, email)
	values ('Nikita', 'Nikita@mail.ru'),
		   ('Denchik', 'Denchik89@mail.ru'),
		   ('Misha', 'Misha@mail.ru'),
	       ('Keril', 'Keril@mail.ru'),
	       ('Vova', 'Vova@mail.ru');

insert into tweet (content, date_publisher, fk_person_id)
values  ('Поднял 20к кубков в бравле за 3 часа', '2022-04-12 12:30:00', 2),
		('Прошел турнир в бравле', '2022-04-14 20:00:00', 2),
		('Месяц выживаю в Вологде', '2022-02-10 12:00:00', 3),
		('Улетел в Грецию из-за санкций', '2022-03-20 8:00:00', 3);

insert into person_to_person
values  (1, 2),
		(1, 3),
		(4, 1),
		(5, 1);

select follow.person_profile as follower_id,
       person.id,
	   person.name,
       sub.name as subscription_name,
       tweet.content as tweet_content,
	   tweet.date_publisher
	   		from person
				left join person_to_person on person.id = person_to_person.person_profile

				left join tweet on tweet.fk_person_id = person_to_person.person_subscription

				left join person as sub on person_to_person.person_subscription = sub.id

				left join person_to_person as follow on person.id = follow.person_subscription

					where person.id = 1 order by tweet.date_publisher asc;