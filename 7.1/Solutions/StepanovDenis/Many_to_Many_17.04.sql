drop table if exists tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists city_to_tour cascade;

create table tour
(
	name text primary key,
	price numeric(15, 3)
);

create table city
(
	name text primary key,
	founded text
);

create table review
(
	id int generated always as identity primary key,
	comment text,
	tour text references tour not null,
	rating int
);

create table city_to_tour
(
	nametour text not null,
	namecity text not null,
	unique(nametour, namecity)
);

alter table city_to_tour
	add constraint fk_namecity_tocity foreign key(namecity) references city(name);

alter table city_to_tour
	add constraint fk_nametour_totour foreign key(nametour) references tour(name);


insert into tour
	values  ('В гостях у Хаски', 4322),
        	('Хиты карелии', 4213);

insert into city
	values  ('Приозерск', '1295'),
	        ('Сартовала', '1468'),
	        ('Валаам', '1407');

insert into city_to_tour
	values ('В гостях у Хаски', 'Приозерск'),
	       ('В гостях у Хаски', 'Сартовала'),
	       ('Хиты карелии', 'Сартовала'),
	       ('Хиты карелии', 'Валаам');

insert into review (comment, tour, rating)
	values ('Отличный тур', 'Хиты карелии', 5),
	       ('Не понравилось', 'Хиты карелии', 2);

select nametour, namecity, comment, rating, price
	from tour
		join city_to_tour  on tour.name = city_to_tour.nametour
		join city on city.name = city_to_tour.namecity
		left join review on tour.name = review.tour;