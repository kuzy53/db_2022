drop table if exists Folder cascade;
drop table if exists File cascade;

create table Folder
(
    id int generated always as identity
        constraint "PK_Folder_id" primary key,
    size numeric,
    name text,
    parentFolder int default,
    unique(name, parentFolder)
);

create table File
(
    id int generated always as identity
        constraint "PK_File_id" primary key,
    size numeric,
    name text,
    parentFolder int not null,
    unique(name, parentFolder)
);

alter table File
	add constraint "FK_FileFolderId"
		foreign key (parentFolder) references Folder (id);

alter table Folder
	add constraint "FK_FolderFolderId"
		foreign key (parentFolder) references Folder (id);

insert into Folder (size, name)
values  (500, 'Homework');

insert into Folder (size, name, parentFolder)
values  (100, 'Algorithms', 1),
		(20, 'Java', 1),
		(30, 'DataBases', 2),
		(5, 'English', 3);

insert into File (size, name, parentFolder)
values  (15, 'script.sql', 2),
		(6, 'data_for_tables.txt', 2),
		(12, 'conditionals.txt', 3),
		(7, 'presentation.ppt',3);

select * from Folder as "parent"
	join folder as "child"
		on "parent".id = "child".parentFolder;

select parent.name as "folder", parent.size,
	child.name as "subfolder", child.size, File.name, File.size
from Folder as "parent"
    left join Folder as "child" on child.parentFolder = parent.id
    left join File on File.parentFolder = child.id;
