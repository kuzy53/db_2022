class Message {
    public int id;
    public String content;
    public Dialog dialog;
}

class MessagePerson extends Message {
    public Person person;
    public String canal;
}

class MessageOperator extends Message {
    public Operator operator;
    public KnowledgeBaseNote knowledgeBaseNote;
}
