import java.util.ArrayList;

class Dialog {
    public int id;
    public Date dateStart;
    public Date dateEnd;
    public ArrayList<MessageOperator> messagesOperators;
    public ArrayList<MessagePerson> messagesPersons;
    public Person person;
    public ArrayList<Operator> operators;
}
