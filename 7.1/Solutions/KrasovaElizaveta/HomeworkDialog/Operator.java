import java.util.ArrayList;

class Operator {
    public int id;
    public String name;
    public ArrayList<Dialog> dialogues;
    public ArrayList<MessageOperator> messages;
}
