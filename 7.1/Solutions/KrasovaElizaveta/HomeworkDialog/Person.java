import java.util.ArrayList;

class Person {
    public int id;
    public String name;
    public ArrayList<Dialog> dialogues;
    public ArrayList<MessagePerson> messages;
}
