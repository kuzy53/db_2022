drop table if exists Folder cascade;
drop table if exists File cascade;

create table Folder
(
    id int generated always as identity
        constraint "PK_Folder_id" primary key,
    size numeric,
    name text,
    parentFolder int default null,
    unique(name, parentFolder)
);

create table File
(
    id int generated always as identity
        constraint "PK_File_id" primary key,
    size numeric,
    name text,
    parentFolder int not null,
    unique(name, parentFolder)
);

alter table File 
	add constraint "FK_FileFolderId" 
		foreign key (parentFolder) references Folder (id);

alter table Folder 
	add constraint "FK_FolderFolderId" 
		foreign key (parentFolder) references Folder (id);

insert into Folder (size, name)
values  (10, 'MyProject');

insert into Folder (size, name, parentFolder) 
values  (25, 'Java', 1), 
		(10, 'Python', 1),
		(6, 'Java', 2), 
		(8, 'Python', 3);
	
insert into File (size, name, parentFolder)
values  (5, 'Collection.class', 2), 
		(10, 'Collection.java', 2),
		(8, 'HelloWorld.py', 3),
		(9, 'GoodbuyWorld.py',3);
	
select * from Folder as "parent" 
	join folder as "child" 
		on "parent".id = "child".parentFolder;
		
select parent.name as "folder", parent.size, 
	child.name as "subfolder", child.size, File.name, File.size
from Folder as "parent"
    left join Folder as "child" on child.parentFolder = parent.id
    left join File on File.parentFolder = child.id or File.parentFolder = parent.id;  
   

 
WITH RECURSIVE folders AS (
	SELECT folder.id, folder.name, folder.parentFolder, file.name AS file_name, 1 AS DEPTH
	FROM folder
		LEFT JOIN file ON file.parentFolder = folder.id
	WHERE folder.id = 1
	UNION
	SELECT f.id, f.name, f.parentFolder, file.name AS file_name, DEPTH + 1 as depth
	FROM Folder AS f
		JOIN folders ON f.parentFolder = folders.id
		LEFT JOIN file ON f.id = file.parentFolder
	WHERE DEPTH + 1 <= 3
)
SELECT *
FROM folders;
   
   
   
   
   
   
   
   
   
   
   
   