drop table if exists Person cascade;
drop table if exists Tweet cascade;
drop table if exists Person_to_Person cascade;

create table Person 
(
    id int generated always as identity primary key,
    name text,
    mail text
);

create table Tweet 
(
    id int generated always as identity primary key,
    contetnt text,
    date_public timestamp,
    person_id int references Person not NULL
);

create table Person_to_Person 
(
    us_profile int references Person not null,  
    us_subscription int references Person not null,
    unique (us_profile, us_subscription)
);

insert into Person (name, mail)
values  ('elizaveta_krasova', 'ea@inbox.ru'),
		('valria.mexico', 'vm@gmail.com'),
		('verevvka', 'vv@mail.ru'),
		('dessbur', 'id@gmail.com'),
		('miroslava', 'me@mail.ru');
	
insert into Tweet (contetnt, date_public, person_id)
values  ('Как выжить в общаге', '2022-04-05 23:25:07', 2),
		('Как сдать Java на 5', '2022-04-02 22:05:45', 2),
		('Как долго можно протянуть на энергетиках', '2022-03-05 13:15:15', 3),
		('Как выспаться за час', '2022-04-01 10:23:55', 3);

insert into Person_to_Person 
values  (1, 2),
		(1, 3),
		(4, 1),
		(5, 1);	
		
select  Person.id,
		Person.name,
		Person_to_Person.us_subscription as subscription_id,
		pp.name as subscription_name,
		Tweet.contetnt as tweet_content,
		ptp.us_profile as follower_id,
		person_follower.name as follower_name
from Person
	left join Person_to_Person on Person.id = Person_to_Person.us_profile
	left join Tweet on Tweet.person_id = Person_to_Person.us_subscription
	left join Person as pp on Person_to_Person.us_subscription = pp.id
	left join Person_to_Person as ptp on Person.id = ptp.us_subscription
	left join Person as person_follower on person_follower.id = ptp.us_profile
	where Person.id = 1
		
		
		
		



