drop table if exists dialog cascade;
drop table if exists dialog_to_operator cascade;
drop table if exists person cascade;
drop table if exists message cascade;
drop table if exists operator cascade;
drop table if exists knowledge_base_note cascade;

create table Person 
(
    id int generated always as identity primary key,
    name text
);

create table Operator
(
    id int generated always as identity primary key,
    name text
);

create table Dialog 
(
    id int generated always as identity primary key,
    date_start timestamp,
    date_end timestamp,
    person_dialog_id int references Person
);

create table Knowledge_base_note
(
    id int generated always as identity primary key,
    base_text text
);

create table dialog_to_operator 
(
    dialog_id int references Dialog not null,
    operator_id int references operator not null,
    unique(dialog_id, operator_id)
);

create table Message 
(
    id int generated always as identity primary key,
    type text,
    canal text,
    content text,
    date_send timestamp,
    person_id int references person,
    dialog_id int references dialog,
    operator_message_id int references operator,
    knowledge_message_id int references knowledge_base_note
);

insert into Person (name)
values  ('Elizaveta');

insert into Operator (name)
values  ('Alica'),
		('Siri'),
		('Okey Google'),
		('Kira');
	
insert into Dialog (date_start, date_end, person_dialog_id)
values  ('2022-04-05 23:25:07', '2023-04-06 23:25:07', 1),
		('2022-04-03 23:25:07', '2023-04-04 23:25:07', 1);

insert into dialog_to_operator (dialog_id, operator_id)
values  (1, 1),
		(1, 2),
		(2, 3),
		(2, 4);

insert into Message (type, canal, content, date_send, person_id, dialog_id)
values  ('person', 'telegram', 'How install git?', '2022-07-06 23:25:07', 1, 1),
		('person', 'telegram', 'I installed git!!!', '2022-08-06 23:25:07', 1, 1),
		('person', 'tiktok', 'Watch video with cats', '2022-10-06 23:25:07', 1, 2),
		('person', 'tiktok', 'This is sooooo cute!!! Realy?', '2022-10-06 23:25:17', 1, 2);

insert into Message (type, content, date_send, operator_message_id, dialog_id)
values 	('operator', 'Think for yourself', '2022-07-06 23:26:07', 1, 1),
		('operator', 'Good job!!!', '2022-08-06 23:27:07', 1, 1),
		('operator', 'Look at the documentation', '2022-07-06 23:26:17', 2, 1),
		('operator', 'Good job!!!', '2022-08-06 23:27:07', 2, 1),
		('operator', 'Ok', '2022-10-06 23:25:57', 3, 2),
		('operator', 'Yes', '2022-10-06 23:25:58', 4, 2);

insert into Knowledge_base_note (base_text)
values  ('Cute cats'),
		('I love cats');

insert into Message (type, knowledge_message_id, date_send, operator_message_id, dialog_id)
values 	('operator', 1, '2022-10-06 23:26:57', 3, 2),
		('operator', 2, '2022-10-06 23::58', 4, 2);

	
SELECT  dialog.id,
		person.name,
		message.content
FROM dialog
	JOIN Person ON dialog.person_dialog_id = Person.id
	join Message on Message.person_id = Person.id
	where dialog.id = 1
union 
SELECT  dialog.id,
		operator.name,
		message.content
FROM message
	join operator on message.operator_message_id = operator.id 
	join dialog on dialog.id = message.dialog_id
	where message.content is not null
	and dialog.id = 1
union
SELECT  dialog.id,
		operator.name,
		base_text
FROM message
	join operator on message.operator_message_id = operator.id
	join dialog on dialog.id = message.dialog_id
	join Knowledge_base_note on message.knowledge_message_id = Knowledge_base_note.id
	where dialog.id = 1











