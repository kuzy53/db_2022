﻿| **ФИО**                           | ДЗ 1:N 30.04 | ДЗ 1:1 30.04 | ДЗ N:M 30.04 | ДЗ Иерархия 30.04 | ДЗ Граф 30.04 | Десериализация 30.04 | SELECT 7.05 |
|-----------------------------------|--------------|--------------|--------------|-------------------|---------------|----------------------|-------------|
| Астраханцев Сергей Константинович |              |              |              |                   |               |                      |             |
| Гафиятуллина Карина Артуровна     |              |              |              |                   |               |                      |             |
| Дубровин Игорь Петрович           | +            | +            |              |                   |               |                      |             |
| Киняшов Егор Евгеньевич           |              |              |              |                   |               |                      |             |
| Лядова Эвелина Романовна          |              |              |              |                   |               |                      |             |
| Малинин Егор Николаевич           |              |              | +            |                   |               |                      |             |
| Мамренко Герман Витальевич        | +            | +            | +            |                   |               |                      |             |
| Мушба Баграт Романович            |              |              |              |                   |               |                      |             |
| Ощепкова Регина Макисмовна        | +            | +            |              |                   |               |                      |             |
| Писцов Андрей Дмитриевчи          |              |              |              |                   |               |                      |             |
| Сычев Леонид Сергеевич            |              |              |              |                   |               |                      |             |
| Труштина Ксения Витальевна        |              |              |              |                   |               |                      |             |
| Шелковкин Иван Дмитриевич         |              |              |              |                   |               |                      |             |
| Яранова Елена Леонидовна          |              |              |              |                   |               |                      |             |
