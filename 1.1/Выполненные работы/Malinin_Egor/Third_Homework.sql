--Tour <-> City: many to many
--Tour <- Review: one to many, Review зависит от Tour
drop table if exists tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists "cityToTour" cascade;

create table tour
(
  name varchar(128) primary key,
  cost integer
);

create table city
(
  name varchar(128) primary key,
  date integer
  
);

create table review
(
  comment varchar(128),
  rating integer,
  tour varchar(128) references tour(name)
);

create table "cityToTour"
(
  "cityName" varchar(128) not null,
  "tourName" varchar(128) not null,
  constraint "FK_cityName_toCity" foreign key("cityName") references city(name),
  constraint "FK_tourName_toTour" foreign key("tourName") references tour(name),
  unique ("cityName","tourName")
  
);

insert into tour
values  ('Алупка',8888),
    ('Горящие туры',9999);

insert into city
values  ('Село', 777),
    ('Деревня',555),
    ('Днепр',666);

insert into "cityToTour"
values  ('Село', 'Алупка'),
    ('Деревня', 'Алупка'),
    ('Деревня', 'Горящие туры'),
    ('Днепр', 'Горящие туры');

insert into review
values  ('Отличный тур', 5, 'Горящие туры'),
    ('Ничего не понравилось', 1, 'Горящие туры');

select "tourName", "cityName", cost, comment, rating from tour 
        left join "cityToTour" on tour.name = "cityToTour"."tourName"
        left  join city on city.name = "cityToTour"."cityName"
        left join review on tour.name = review.tour;
