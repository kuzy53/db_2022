create table users
(
	nickname text,
	first_name text,
	last_name text
);

create table settings
(
	font_size int,
	color_scheme text,
	nick text
);

alter table users
    add primary key (nickname);

alter table settings
    add foreign key (nick) references users (nickname);

insert into users 
values ('100gramoВИЧ', 'Михаил', 'Михайлов'),
       ('100pudoff', 'Сергей', 'Опасный');
      
insert into settings
values (20, 'Green', '100gramoВИЧ'),
       (300, 'Pink', '100pudoff');
      
select *
from users
         join settings on nickname = nick;
