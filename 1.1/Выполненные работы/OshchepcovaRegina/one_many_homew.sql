create table tour
(
	name text ,
	price numeric(15,6)
);

create table review
(
	comment text,
	ratio int,
	"tourName" text
);

alter table tour  
	add constraint "NAME" 
		primary key (name);
	
alter table review
	add constraint "T_Name" 
		foreign key ("tourName") references tour;
	
insert into tour  (name, price)
values ('Жемчужины Кыргызстана', '68417'),
		('Курортный Памир', '82695')
		
insert into review (comment, ratio, "tourName")
values ('Бешбармак был очень вкусным! ', '22', 'Жемчужины Кыргызстана'),
		('Лошадь не еда, лошадь-друг', '0', 'Курортный Памир'),
		('Великая Китайская Партия одобряет', '23', 'Жемчужины Кыргызстана'),
		('Все супер-дупер', '10', 'Курортный Памир')	

select * from tour;
select * from review;

select name, price, comment, ratio
from tour left join review on name = "tourName" 
