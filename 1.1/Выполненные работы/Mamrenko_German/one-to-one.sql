drop table if exists user_ cascade;
drop table if exists settings cascade;

create table user_
(
	nickname text,
	first_name text,
	last_name text
);

create table settings
(
	font_size int,
	color_scheme text,
	nick text
);

alter table user_
    add primary key (nickname);

alter table settings
    add foreign key (nick) references user_ (nickname);

alter table settings
    add unique (nick);

insert into user_
values ('Jaspasca', 'Анатолий', 'Робинович'),
       ('NightShade', 'Михаил', 'Уткин');
      
insert into settings
values (14, 'Blue', 'Jaspasca'),
       (12, 'Yellow', 'NightShade');
      
select *
from user_
         join settings on nickname = nick;
