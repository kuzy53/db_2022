drop table if exists tour cascade;
drop table if exists review cascade;

create table tour
(
	name text,
	price numeric
);

create table review
(
	id bigint generated always as identity primary key,
	comm text,
	rating int,
	tour_name text
);

alter table tour
    add primary key (name);

alter table review
    add foreign key (tour_name) references tour (name);
   
insert into tour (name, price)
values ('Прогулки по ночному Эдо', '46000'),
       ('Венецианский карнавал', '35000');

insert into review  (comm, rating, "tour_name")
values ('Очень понравилось, цветущая сакура была великолепна!', '5', 'Прогулки по ночному Эдо'),
       ('А мне в Нагасаки больше понравилось, хотя в Эдо тоже хорошо.', '4', 'Прогулки по ночному Эдо'),
       ('Было шумно и весело, загадочные маски, романтика.', '5', 'Венецианский карнавал'),
       ('Потерял маску и пришлось доплачивать на входе, очень расстроился.', '3', 'Венецианский карнавал');

select *
from tour
         join review on name = tour_name;
