drop table if exists tour cascade;
drop table if exists review cascade;
drop table if exists city cascade;
drop table if exists city_to_tour cascade;

create table tour
(
	name_t text constraint "PK_name_tour" primary key,
	price numeric
);

create table review
(
	id bigint generated always as identity primary key,
	comm text,
	rating int,
	tour text constraint "FK_tour_review" references tour not null
);

create table city
(
	name_c text constraint "PK_name_city" primary key,
	founded int
);

create table city_to_tour
(
    city_name text not null,
    tour_name text not null,
    constraint "FK_name_tour_to_tour" foreign key(tour_name) references tour,
    constraint "FK_name_city_to_city" foreign key(city_name) references city
);

alter table city_to_tour
    add constraint key_city_to_tour unique(city_name, tour_name);
    
insert into tour (name_t, price)
values('В гостях у хаски', '5000'),
      ('Хиты карелии', '8000');

insert into city (name_c, founded)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);
      
insert into city_to_tour
values  ('Приозерск', 'В гостях у хаски'),
        ('Сортавала', 'В гостях у хаски'),
        ('Сортавала', 'Хиты карелии'),
        ('Валаам', 'Хиты карелии');
       
insert into review (comm, rating, tour)
values('Отличный тур!', '5', 'Хиты карелии'),
      ('Не понравилось.', '2', 'Хиты карелии');
   
select tour_name, city_name, founded, comm, rating
from tour
    join city_to_tour on tour.name_t = city_to_tour.tour_name
    join city on city.name_c = city_to_tour.city_name
    left join review on tour.name_t = review.tour