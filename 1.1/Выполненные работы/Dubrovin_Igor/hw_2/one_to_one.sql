drop table if exists users cascade;
drop table if exists settings cascade;

create table users
(
    id         int generated always as identity primary key,
    nickname   text,
    first_name text,
    last_name  text
);

create table settings
(
    front_size   int,
    color_scheme text,
    id_user      int references users not null unique
);


insert into users(nickname, first_name, last_name)
values ('dubok27', 'Igor', 'Dubrovin'),
       ('krasova03', 'Elizaveta', 'Krasova');

insert into settings
values (1024, 'blue', 1),
       (128, 'pink', 2);



select nickname, first_name, last_name, front_size, color_scheme
from users
         join settings on users.id = settings.id_user;