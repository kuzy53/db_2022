import psycopg2, os, time
import itertools, typing
from psycopg2.extras import RealDictCursor

# os.system('sudo docker start my-postgres')

conn = psycopg2.connect("dbname=hw_two user=dessbur password=dessbur2002 host=localhost port=999",
                        cursor_factory=RealDictCursor)


def one_to_one():
    class Settings:
        def __init__(self, front_size: int, color_scheme: str):
            self.front_size = front_size
            self.color_scheme = color_scheme

    class User:
        def __init__(self, nickname: str, first_name: str, last_name: str, setting: Settings):
            self.nickname = nickname
            self.first_name = first_name
            self.last_name = last_name
            self.setting = setting

    cur = conn.cursor()
    query = """
    select nickname, first_name, last_name, front_size, color_scheme
from users
    join settings on users.id = settings.id_user;
        """
    cur.execute(query)
    result = cur.fetchall()
    result.sort(key=lambda row: row['nickname'])
    users = []
    for key, group in itertools.groupby(result, lambda row: row['nickname']):
        entries = list(group)
        setting = Settings(
            entries[0]['front_size'], entries[0]['color_scheme'])
        user = User(entries[0]['nickname'], entries[0]['first_name'], entries[0]['last_name'], setting)
        users.append(user)
    return users


users = one_to_one()
for user in users:
    print(f'for {user.first_name} {user.last_name}, nickname: {user.nickname} \n '
          f'setting: size - {user.setting.front_size}, color scheme: {user.setting.color_scheme}')
    print('-' * 10)
