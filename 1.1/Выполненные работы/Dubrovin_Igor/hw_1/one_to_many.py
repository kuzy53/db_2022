import psycopg2, os, time
import itertools, typing
from psycopg2.extras import RealDictCursor

# os.system('sudo docker start my-postgres')

conn = psycopg2.connect("dbname=one_to_many user=dessbur password=dessbur2002 host=localhost port=999",
                        cursor_factory=RealDictCursor)


def one_to_many():
    class Tour:
        def __init__(self, name: str):
            self.name = name
            self.review: typing.List[Review] = []

    class Review:
        def __init__(self, comment: str, rating: str, tour: Tour):
            self.comment = comment
            self.rating = rating
            self.tour = tour

    cur = conn.cursor()
    query = """
    select name as name_tour, comment, rating
from tour
         left join review on tour_name = name;
        """
    cur.execute(query)
    result = cur.fetchall()
    result.sort(key=lambda row: row['name_tour'])
    toures = []
    for key, group in itertools.groupby(result, lambda row: row['name_tour']):
        reviews = list(group)
        tour = Tour(reviews[0]['name_tour'])
        toures.append(tour)
        for rev in reviews:
            review = Review(
                rev['comment'], rev['rating'], tour)
            tour.review.append(review)
    return toures


tours = one_to_many()
for tour in tours:
    print(f'Name Tour: {tour.name}')
    for eq in tour.review:
        print(f'comment: {eq.comment};\nrating: {eq.rating}')
    print('-' * 10)
