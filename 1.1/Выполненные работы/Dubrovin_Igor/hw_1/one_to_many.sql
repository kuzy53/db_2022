drop table if exists tour cascade;
drop table if exists review cascade;

create table tour
(
    name  text primary key,
    price int
);

create table review
(
    id        int generated always as identity primary key,
    comment   text,
    rating    int,
    tour_name text references tour
);


insert into tour
values ('Altay', 11900),
       ('Sochi', 26999),
       ('Kavkaz', 3999);


insert into review(comment, rating, tour_name)
values ('Nice, i love it', 1, 'Altay'),
       ('This was awful, Yestoday, I could die', 0.1, 'Kavkaz'),
       ('Nothing interesting, but very safely', 0.7, 'Sochi'),
       ('Today i had new girl, tomorow we`ll marry', 1, 'Sochi'),
       ('Sometimes I love my children, but I wan relax', 0.4, 'Altay'),
       ('Alah Acbar', 0.9, 'Kavkaz');

select name as name_tour, comment, rating
from tour
         left join review on tour_name = name;